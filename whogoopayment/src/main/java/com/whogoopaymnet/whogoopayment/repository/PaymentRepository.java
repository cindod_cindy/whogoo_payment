package com.whogoopaymnet.whogoopayment.repository;

import com.whogoopaymnet.whogoopayment.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {
}
