package com.whogoopaymnet.whogoopayment.controller;

import com.whogoopaymnet.whogoopayment.dto.PaymentRequest;
import com.whogoopaymnet.whogoopayment.model.Payment;
import com.whogoopaymnet.whogoopayment.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;


@Slf4j
@RestController
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @PostMapping("/get-payment-user")
    public List<Payment> getAllPayment() {

        return paymentService.getAllPaymentData();
    }

    @PostMapping("/posts-payment-user")
    public Payment createUserPayment(@Valid @RequestBody PaymentRequest paymentRequest) {
        return paymentService.createPaymentRequest(paymentRequest);
    }

    @PostMapping("/update/payment-user/{id}")
    public Payment updateUserPayment(@PathVariable Long id, @Valid @RequestBody PaymentRequest paymentRequest) {
        return   paymentService.updatePayment(id,paymentRequest);
    }


    @PostMapping("/delete/payment-user/{id}")
    public ResponseEntity<?> deletePaymentUSer(@PathVariable Long id) {
        return paymentService.deletePayment(id);

    }

    @PostMapping("/get-by-id/payment-user/{id}")
    public Optional<Payment> getPaymentById(@PathVariable Long id){
        return  paymentService.getPaymentById(id);
    }


}
