package com.whogoopaymnet.whogoopayment.service;

import com.whogoopaymnet.whogoopayment.dto.PaymentRequest;
import com.whogoopaymnet.whogoopayment.exception.ResourceNotFoundException;
import com.whogoopaymnet.whogoopayment.model.Payment;
import com.whogoopaymnet.whogoopayment.repository.PaymentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    public List<Payment> getAllPaymentData() {
        return paymentRepository.findAll();
    }


    public Payment createPaymentRequest(PaymentRequest paymentRequest) {
        var payment = Payment.builder()
                .userName(paymentRequest.getUserName())
                .namaAkun(paymentRequest.getNamaAkun())
                .bank(paymentRequest.getBank())
                .build();
        return (Payment) paymentRepository.save(payment);
    }

    public Optional<Payment> getPaymentById(Long idPayment){
        return paymentRepository.findById(idPayment);
    }

    public Payment updatePayment(Long paymentId,  PaymentRequest paymentRequest) {
        return paymentRepository.findById(paymentId).map(payment -> {
            payment.setUserName(paymentRequest.getUserName());
            payment.setNamaAkun(paymentRequest.getNamaAkun());
            payment.setBank(paymentRequest.getBank());
            return paymentRepository.save(payment);
        }).orElseThrow(() -> new ResourceNotFoundException("PostId " + paymentId + " not found"));
    }



    public ResponseEntity<?> deletePayment(Long paymentId) {
        return paymentRepository.findById(paymentId).map(payment -> {
            paymentRepository.delete(payment);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("PostId " + paymentId + " not found"));
    }

}
